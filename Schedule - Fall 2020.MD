## CS-490 Software Engineering 
### Instructor: Dr. Heidi Ellis
### Topics and Assignments Will Change
### Updated: 2020-08-20
### Schedule for CS 490 Software Engineering

<doctype html="">
<div id="boundary" style="margin-right:-2px;">
<table id="header" width="100%" border="3">
<tbody>
<!================= WEEK 1 =========================!>

<tr>
<td colspan="5" bgcolor="#339999"><center>Week 1: Orientation</center></td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 9/1 (in person)</td>
<td>- [What is Software Engineering? - Georgia Tech](https://www.youtube.com/watch?v=gBd-ct58DCI) </td>
<td>- Course Orientation <br />- Introduction to SE 
      <br />- [Opening Thoughts](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Slides/Opening_Thoughts.pdf)</td>
<td> 
</td>
<td><span style="color: #003366;"></span>- [Homework 1 - What is Software Engineering](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Homeworks/Homework_1_-_What_is_SE.doc) <b>Due:</b> 9:30 a.m. Tuesday 9/8 <b>Submit:</b> Individual Word or ODT document via Kodiak (no late days)</td>
</tr>
<tr>
<td>Thurs 9/3 (online)</td>
<td>- Fred Brooks "No Silver Bullet" which may be accessed [in PDF](http://worrydream.com/refs/Brooks-NoSilverBullet.pdf) 
or [in HTML](http://www.cs.nott.ac.uk/%7Epszcah/G51ISS/Documents/NoSilverBullet.html)
  <br />- [Tuckman's stages of group development](https://en.wikipedia.org/wiki/Tuckman%27s_stages_of_group_development)</td>
<td>- [Team Formation Slides](http://mars.wne.edu/%7Ehellis/CS490/Slides/Team%20Slides.pdf) 
<br />- [Meeting Note Format](http://mars.wne.edu/%7Ehellis/CS490/Project/Meeting%20Note%20Format.xlsx)
<br />- [Triangles and Teams Activity](https://drive.google.com/drive/folders/1jP0VEHHanBm0gEHtAzXrUsb4zaEcL1TJ)</td>
<td></td>
<td>- [Team Assn 1 - Launch Report](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Team%20Assignments/Launch_Report.doc) <b>Due:</b> 9:30 a.m. Thursday 9/10 <b>Submit:</b> Team Word or ODT document via Kodiak</td>
</tr>
<!================= WEEK 2 =========================!>
<tr>
<td colspan="5" bgcolor="#339999">
<center>Week 2: Software Development</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 9/8</td>
<td>-[What is the SDLC? (text)](https://blog.testlodge.com/software-development-life-cycle/)
<br />- [What is the SDLC? (video)](https://www.youtube.com/watch?v=cgWzYMaDncI)
<br />- [Steps of the SDLC (video)](https://www.youtube.com/watch?v=gNmrGZSGK1k)
<td>- [Software Development Activity](https://drive.google.com/drive/folders/1jP0VEHHanBm0gEHtAzXrUsb4zaEcL1TJ)
</td>
<td>- Homework 1 - What is Software Engineering? (no late days)</td>
<td>
- [Homework 2 - FOSS Field Trip](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Homeworks/Homework_2_-_FOSS_Field_Trip.doc) <b>Due:</b> 9:30 a.m. Tuesday 9/15 <b>Submit:</b> Individual Word or ODT document via Kodiak

</td>
</tr>
<tr>
<td>Thurs, 9/10</td>
<td>- [A Rational Design Process (text)](https://users.ece.utexas.edu/%7Eperry/education/SE-Intro/fakeit.pdf)
<br />- [Different Project LifeCycles (video)](https://www.youtube.com/watch?v=JFi5dfsTR10)
<br />- [Waterfall model (video)](https://www.youtube.com/watch?v=5A5XCuWMG4o)
<br />- [Iterative and Incremental model (video)](https://www.youtube.com/watch?v=gcpOZi6Hz38&t=9s)
<br />- [Sprial model (video)](https://www.youtube.com/watch?v=mp22SDTnsQQ)
<br />- [Scrum model (video)](https://www.youtube.com/watch?v=rGhRMzz0WVU)
</td>
<td>- [Software Development Lifecycles Activity](https://drive.google.com/drive/folders/1jP0VEHHanBm0gEHtAzXrUsb4zaEcL1TJ)</td>
<td>- Team Assn 1 - Launch Report
</td>
<td><br />
- [Team Assn 2 - BNM GitLab Exploration](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Team%20Assignments/BNM_GitLab_Exploration.doc) <b>Due:</b> 9:30 a.m. Thursday 9/17 <b>Submit:</b> Team Word or ODT document via Kodiak <b>submit part 2 as separate PDF</b></td>
</tr>
<!================= WEEK 3 =========================!>
<tr>
<td colspan="5" bgcolor="#339999">
<center>Week 3: FOSS Landscape</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 9/15</td>
<td>- [Open Source Software Basics (there are cookies!)](https://www.youtube.com/watch?v=Tyd0FO0tko8&feature=youtu.be) 
<br/>- [Introduction to Free and Open Source Software](http://teachingopensource.org/practical-oss-exploration/introduction-free-open-source-software/) 
<br/>- [What is Free Software](http://www.gnu.org/philosophy/free-sw.html) from the Free Software Foundation 
<br />- [The Open Source Definition](http://opensource.org/osd) from the Open Source Initiative</td>
<td>- [Communication in Projects Activity](https://drive.google.com/drive/folders/1jP0VEHHanBm0gEHtAzXrUsb4zaEcL1TJ)
</td>
<td>- Homework 2 - FOSS Field Trip
</td>
<td>
- [Homework 3 - Issue Review](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Homeworks/Homework_3_-_Issue_Review.doc) <b>Due:</b> 9:30 a.m. Tuesday 9/22 <b>Submit:</b> Individual Word or ODT document via Kodiak (no late days)
<br />
- Team Assn 3 - Assess BNM against CII rubric (Compare to last year's results?)
</td>
</tr>
<tr>
<td>Thurs, 9/17</td>
<td>- [Chapter 3 The Lay of the Land](http://teachingopensource.org/practical-oss-exploration/the-lay-of-the-land/) from Practical OSS Exploration 
<br/>
- [Interacting with a FOSS community](https://www.snoyman.com/blog/2017/10/effective-ways-help-from-maintainers "Interacting with a FOSS Community")
</td>
<td>
- Review graphics from GitLab BNM Exploration
<br />
- [FOSS Community principles](https://drive.google.com/drive/folders/1E3PjxXJ1YM6Ma6r7HDBSfp1wYoOmp5BA)
</td>
<td>- Team Assn 2 - GitLab BNM Exploration </td>
<td>Homework 4 - Install BNM</td>
</tr>
<tr>
<!================= WEEK 4 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 4: Requirements</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues 9/22</td>
<td></td>
<td>
- <b>Quiz 1</b>
<br />
- [FOSS Community Scenarios](https://drive.google.com/drive/folders/1iZKa_ylX3XK-aMio6-WRiuFS2astWRIc)
<br /> 
</td>
<td>- Homework 3 - Issue Review</td>
<td></td>
</tr>
<tr>
<td>Thurs 9/24</td>
<td>
- [Writing a Software Requirements Document by Tanya Berezin](https://home.adelphi.edu/%7Esiegfried/cs480/ReqsDoc.pdf)
<br /> 
- [Cafeteria Ordering SRS](http://csis.pace.edu/%7Emarchese/SE616_New/Samples/SE616_SRS.doc) by Karl Weigers</td>
<td>
- [Requirements Exploration](https://drive.google.com/drive/folders/1h5WpO6y-xTLIrAQS-LCHx0fKyJAxG7Kg)
<br />
- Installation Troubleshooting
<br />
</td>
<td></td>
<td></td>
</tr>
<tr>
<!================= WEEK 5 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 5: Requirements cont.</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 9/29</td>
<td>
- [Writing Quality Requirements](http://www.processimpact.com/articles/qualreqs.html) by Karl Wiegers 
<br />
- [5 Ways to Write Clearer Requirements](http://www.bridging-the-gap.com/5-ways-to-write-clearer-requirements/) by Laura Brandenburg (bold items and tables)</td>
<td>[SRS Activity](https://drive.google.com/drive/folders/1iIrqSujlgmtgAt6qQIKMvf1X51lO9RKm)
</td>
<td>Homework 4 - Install BNM</td>
<td>Team Assn 4 - Write or assess requirements</td>

</tr>
<tr>
<td>Thurs, 10/1</td>
<td></td>
<td>Review Activity</td>
<td>Team Assn 4.1 - Come up with list of questions for Kristen about BNM</td>
<td>Homework 5 - GitLab Workflow</td>
</tr>
<tr>
<!================= WEEK 6 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 6: Customer Interviews and Reviews</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 10/6</td>
<td>
- [Software Reviews](http://en.wikipedia.org/wiki/Software_review) from Wikipedia 
<br />
- [Improving Quality Through Software Inspections](http://www.processimpact.com/articles/inspects.html) by Karl E. Wiegers 
<br />
- [Seven Deadly Sins of Software Reviews](http://www.processimpact.com/articles/revu_sins.html) by Karl E. Wiegers
</td>
<td>Reviews</td>
<td style="height: 114px;">Team Assn 3 - Assess BNM against CII</td>
<td></td>
</tr>
<tr>
<td>Thurs, 10/8</td>
<td></td>
<td>Meet with Kristen (dependent on schedule)</td>
<td></td>
<td><span style="color: #003366;"></span>Team Assn 5 - Review Checklist applied to requirements before class on Thursday</td>
</tr>
<tr>
<!================= WEEK 7 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 7: Reviews and Finalize Requirements</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 10/13</td>
<td> </td>
<td>In-class Reviews
<br />
- Quiz 2 
</td>
<td>Homework 5 - GitLab Workfow</td>
<td>Team Assn 4 - Review Report</td>
</tr>
<tr>
<td style="height: 61px;">Thurs, 10/15</td>
<td></td>
<td> Consolidate Requirements</td>
<td></td>
<td></td>
</tr>
<tr>
<!================= WEEK 8 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 8: Software Architectures</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues 10/20 <font color="red">ATO</font></td>
<td>[Three Flaws in Software](http://www.youtube.com/watch?v=JOAq3YN45YE) Part 1 - Background [Three Flaws in Software](http://www.youtube.com/watch?v=eCg_VoFW46s) Part 2 - Background [Three Flaws in Software](http://www.youtube.com/watch?v=0wxyOng0-14) Part 3 - Background [Three Flaws in Software](http://www.youtube.com/watch?v=fjpkPe6-580) Part 4 - Background</td>
<td>Design Activity</td>
<td>Design Review Checklist<span style="color: #003366;"></span></td>
<td>Team Assn 5 - Design - have them create an architecture diagram and use one of the known design notations to represent the existing design?</td>
</tr>
<tr>
<td>Thurs, 10/22</td>
<td> [Software Architecture](http://www.thomasalspaugh.org/pub/fnd/architecture.html) by Thomas A. Alspaugh [13 Architectural Styles](http://www.ccs.neu.edu/home/lieber/courses/csg110/sp08/lectures/april7/rosenblum-implicit-invoc.pdf) David S. Rosenblum [Software Architecture](http://msdn.microsoft.com/en-us/library/ee658098.aspx) from MSN</td>
<td>Software Architecture Activity</td>
<td>Team Assn 4 - Review Report Final Version of Requirement Due</td>
<td> <span style="color: #003366;"></span>**</td>
</tr>
<tr>
<td colspan="5" bgcolor="#339999">
<!================= WEEK 9 =========================!>
<center>Week 9: Design</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%"></th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues 10/27</td>
<td></td>
<td>Customer Meeting</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Thurs, 10/29</td>
<td></td>
<td>Design Reviews</td>
<td>  Team Assn 5 - Design (in some form)</td>
<td> <span style="color: #003366;"></span></td>
</tr>
<tr>
<td colspan="5" bgcolor="#339999">
<!================= WEEK 10 =========================!>
<center>Week 10: Design Reviews and Testing</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues 11/3</td>
<td></td>
<td>Testing the Program White Box Testing Activity</td>
<td></td>
<td>Team Assn 6 - Testing</td>
</tr>
<tr>
<td>Thurs 11/5</td>
<td>[Requirements Based Testing](http://www.professionalqa.com/requirements-based-testing "RBT") from Project Management Consulting</td>
<td>
<b>Quiz 3</b>
<br />
Requirements Based Testing Activity- HE: Create this. Have students write BDD test cases or review existing ones?</td>
<td></td>
<td></td>
</tr>
<tr>
<td colspan="5" bgcolor="#339999">
<!================= WEEK 11 =========================!>
<center>Week 11: Testing</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 11/10</td>
<td>[System Testing](http://www.guru99.com/system-testing.html) from Guru99 [System Testing](http://en.wikipedia.org/wiki/System_testing) from Wikipedia [A Beginners Guide to System Test](http://www.softwaretestinghelp.com/system-testing/) by Software Testing Help</td>
<td>System Testing Activity</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Thurs, 11/12</td>
<td> </td>
<td>More Testing</td>
<td></td>
<td></td>
</tr>
<tr>
<!================= WEEK 12 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 12: More Testing</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 11/17</td>
<td><span style="color: red;"></span></td>
<td>In-class meeting - keep meeting notes</td>
<td>Team Assn 6 - Testing</td>
<td> </td>
</tr>
<tr>
<td>Thurs, 11/19</td>
<td>[Toptal's Introduction to Software Estimation](https://www.toptal.com/agile/software-costs-estimation-in-agile-project-management) <a>Read only to the section titled "Our approach to software costs and pricing"</a>[Overview of CoCoMo Model](http://www.softstarsystems.com/overview.htm) [-](http://www.guru99.com/system-testing.html)</td>
<td>Cost Estimation Activity</td>
<td></td>
<td>Team Assn 7 - Make a contribution - HE: Figure out what this would look like</td>
</tr>
<tr>
<!================= WEEK 13 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 13: Estimation - ONLINE</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th style="height: 19px;" width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 11/24</td>
<td>- [ACM Code of Ethics and Professional Practice](http://www.acm.org/about-acm/acm-code-of-ethics-and-professional-conduct)</td>
<td>Ethics Activity/Diversity
<br />
<b>Quiz 4</b>
</td>
<td> </td>
<td> [Student Evaluation of Project and Team](./Project/Student%20Evaluation%20of%20Project%20and%20Team.doc)<span style="color: #003366;"></span>**Due:** 5:00 PM Mon 12/9 **Submit:** One per person, email to Dr. Ellis</td>
</tr>
<tr>
<td>Thurs, 11/26</td>
<td><font color="red">Thanksgiving - No Class!!</font></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<!================= WEEK 14 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 14: Presentations and Wrap Up - ONLINE</center>
</td>
</tr>
<tr bgcolor="#339999">
<th width="8%" valign="top" align="center">Date</th>
<th width="25%" valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 12/1</td>
<td>- [The Business Case for Better Software Practices](https://docplayer.net/17964994-Business-case-for-better-software-practices.html)</td>
<td>Why Software Engineering? Wrap Up</td>
<td></td>
<td> </td>
</tr>
<tr>
<td>Thurs, 12/3</td>
<td style="height: 57px;"> </td>
<td>Final Project Presentations Must follow the [Presentation Guidelines](./Project/Presentation%20Guidelines.pdf)</td>
<td>Team Assn 7 - Make a Contribution</td>
<td> </td>
</tr>
<!================= WEEK 15 =========================!>
<tr>
<td colspan="5" bgcolor="#339999">
<center>Week 15: </center>
</td>
</tr>
<tr bgcolor="#339999">
<th width="8%" valign="top" align="center">Date</th>
<th width="25%" valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
<tr>
<td>Mon 12/7</td>
<td> </td>
<td> </td>
<td> Student Evaluation of Project and Team</td>
<td> </td>
</tr>

</tbody>
</table>
</div>
</doctype>